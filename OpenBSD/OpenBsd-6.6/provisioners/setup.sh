#!/bin/sh
set -e
set -x

# setup custom keyboard
wsconsctl -w keyboard.encoding=fr

pkg_add ansible-${ANSIBLE:-2.7.9} py3-pip-${PY3_PIP:-9.0.3} ansible-lint-${ANSIBLE_LINT:-4.1.0}
PIP=pip`python3 --version | cut -f2 -d' ' | cut -f 1 -f 2 -d'.'`
$PIP install testinfra==${TESTINFRA:-2.0.0}

# install tools
pkg_add vim-8.1.2061-no_x11
pkg_add wget-1.20.3p1
pkg_add rsync-${RSYNC:-3.1.2p0}

# Add VAGRANT_USER user
groupadd $VAGRANT_GROUP
useradd -g $VAGRANT_GROUP -p $(encrypt $VAGRANT_PASSWORD) -m $VAGRANT_USER
mkdir -pm 700 /home/$VAGRANT_USER/.ssh
ftp -o - https://raw.githubusercontent.com/mitchellh/vagrant/master/keys/vagrant.pub >> /home/$VAGRANT_USER/.ssh/authorized_keys
chmod 0600 /home/$VAGRANT_USER/.ssh/authorized_keys
chown -R $VAGRANT_USER:$VAGRANT_USER /home/$VAGRANT_USER/.ssh
echo "permit nopass keepenv $VAGRANT_USER as root" >> /etc/doas.conf

# Configuring sshd
wget http://${PACKER_HTTP_ADDR}/sshd_config -O /tmp/sshd_config
cp /etc/ssh/sshd_config /etc/ssh/sshd_config.original
cp /tmp/sshd_config /etc/ssh/
rcctl restart sshd

# Configure packet filter
# wget http://${PACKER_HTTP_ADDR}/pf.conf -O /tmp/pf.conf
# cp /etc/pf.conf /etc/pf.conf.original
# cp /tmp/pf.conf /etc/pf.conf
# pfctl -d
# pfctl -e

echo "Zeroing-out the free space"
dd if=/dev/zero of=/EMPTY bs=1M  || echo "dd exit code $? is suppressed"
rm -f /EMPTY
