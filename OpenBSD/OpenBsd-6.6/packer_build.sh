#!/bin/sh
export PACKER_SSH_KEY_FOLDER=$KEYPATH/sandbox/
export BUILD_FOLDER=~/Documents/atelier/vm_builds

echo "Removing the old box..."
vagrant destroy
rm $BUILD_FOLDER/Openbsd.box
vagrant box remove openbsd
echo "Done !"

echo "Importing ssh keys from $PACKER_SSH_KEY_FOLDER"
cp "$PACKER_SSH_KEY_FOLDER"/*.pub ./http/
echo "Done !"

packer validate openbsd.json && \
packer build openbsd.json && \
echo "Adding the box openbsd to vagrant..." && \
vagrant box add $BUILD_FOLDER/Openbsd.box --name "openbsd" && \
echo "Done !"

echo "Removing the ssh keys from the project..."
rm ./http/*.pub
echo "Done !"
