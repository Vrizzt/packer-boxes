# Packer boxes

## Contents

* [alpine]- Alpine Linux templates (x86_64 and
  x86)
  * alpine-3.12 - Alpine Linux 3.12.0 templates
* [debian] - Debian templates (amd64)
  * debian-10 - Debian 10.4 templates
* [openbsd]- OpenBSD templates (amd64 and i386)
  * openbsd-6.6 - OpenBSD 6.6 templates
  * [more](openbsd/README.mdown)

## Licensing

All the files in this directory and descendants are provided under the
Apache License, Version 2.0 (the "License"); unless otherwise
explicitly stated.  You may not use these files except in compliance
with the License.  You may obtain a copy of the License at

   <http://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
implied.  See the License for the specific language governing
permissions and limitations under the License.

- - -

Copyright &copy; 2014-2020 Upperstream Software.
