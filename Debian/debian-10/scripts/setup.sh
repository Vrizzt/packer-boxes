#!/usr/bin/env bash

# Base
if [ ${OPTIMISE_REPOS:-0} -eq 1 ]; then
	sed 's=http://http.us.debian.org/=http://cdn.debian.net/=' /etc/apt/sources.list > /tmp/sources.list
	mv /tmp/sources.list /etc/apt/sources.list
	# apt-get update
fi

test -z "$WGET" && apt-get install -y curl
sed -i "s/^.*requiretty/#Defaults requiretty/" /etc/sudoers

# Linux headers
# apt-get -y install linux-headers-`uname -r`

# Add VAGRANT_USER user
mkdir -pm 700 /home/$VAGRANT_USER/.ssh
${WGET:="curl -kL"} 'https://raw.github.com/mitchellh/vagrant/master/keys/vagrant.pub' >> /home/$VAGRANT_USER/.ssh/authorized_keys
chmod 0600 /home/$VAGRANT_USER/.ssh/authorized_keys
chown -R $VAGRANT_USER:$VAGRANT_USER /home/$VAGRANT_USER/.ssh
echo "$VAGRANT_USER ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers.d/$VAGRANT_USER
chmod 0440 /etc/sudoers.d/$VAGRANT_USER

# Virtualbox
# test "${INSTALL_DKMS:-false}" = "true" && apt-get -y install dkms
# if [ -z "$VBOX_VER" ]; then
#     VBOX_VER=`cat /root/.vbox_version`
# fi
# test "$VIRTUALBOX_WITH_XORG" = "1" || VIRTUALBOX_WITHOUT_XORG=--nox11
# wget http://download.virtualbox.org/virtualbox/$VBOX_VER/VBoxGuestAdditions_$VBOX_VER.iso
# mount -t iso9660 VBoxGuestAdditions_$VBOX_VER.iso /media/cdrom0
# if { yes | /media/cdrom0/VBoxLinuxAdditions.run "$@" $VIRTUALBOX_WITHOUT_XORG; }; then
#     :
# else
#     rc="$?"
#     if [ "$rc" = "2" ]; then
#         echo "Assume VirtualBox Guest Additions should work after reboot and ignore this error."
#     else
#         exit $rc
#     fi
# fi
# rm -f VBoxGuestAdditions_*.iso

# Configuring sshd
wget http://${PACKER_HTTP_ADDR}/sshd_config -O /tmp/sshd_config
cp /etc/ssh/sshd_config /etc/ssh/sshd_config.original
cp /tmp/sshd_config /etc/ssh/
systemctl restart sshd
