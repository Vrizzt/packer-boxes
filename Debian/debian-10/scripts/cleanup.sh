#!/usr/bin/env bash

## ----------------------------------------------------------------------------
# Cleanup the VM after packer finished the build
# Variables needed : none
## ----------------------------------------------------------------------------


DISK_USAGE_BEFORE_CLEANUP=$(df -h)

echo "Removing unused apt packages and leftovers"
apt -y autoremove --purge
apt -y clean
apt -y autoclean

echo "Removing /tmp/*"
rm -rf /tmp/*

echo "Removing dhcp leases"
rm -rf /var/lib/dhcp/* || true

echo "Removing logs"
find /var/log -type f | while read f; do echo -ne '' > ${f}; done;

echo "Removing last login information"
>/var/log/lastlog
>/var/log/wtmp
>/var/log/btmp

echo "Removing bash history"
unset HISTFILE
rm ~/.bash_history

# Disk cleanup
echo "$(date): Filling the remaining disk space with 0x00.  This will take long (appr. 15 minutes per 100GB)."
sudo dd if=/dev/zero of=/tmp/zero.bin
sudo rm /tmp/zero.bin
rm -f /root/VBoxGuestAdditions*
echo "$(date): Disk cleanup completed."

# NOTE: Packer does not sync before shutdown
sync

echo "Disk usage before cleanup"
echo "${DISK_USAGE_BEFORE_CLEANUP}"
echo "Disk usage after cleanup"
df -h
