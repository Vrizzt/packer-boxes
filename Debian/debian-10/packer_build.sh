#!/bin/sh
export PACKER_SSH_KEY_FOLDER=$KEYPATH/sandbox/
export BUILD_FOLDER=~/Documents/atelier/vm_builds

echo "Removing the old box..."
vagrant destroy
rm $BUILD_FOLDER/debian.box
vagrant box remove debian
echo "Done !"

# echo "Importing ssh keys from $PACKER_SSH_KEY_FOLDER"
# cp "$PACKER_SSH_KEY_FOLDER"/*.pub ./http/
# echo "Done !"

packer validate debian.json && \
packer build debian.json && \
echo "Adding the box debian to vagrant..." && \
vagrant box add $BUILD_FOLDER/debian.box --name "debian" && \
echo "Done !"

echo "Removing the ssh keys from the project..."
rm ./http/*.pub
echo "Done !"
