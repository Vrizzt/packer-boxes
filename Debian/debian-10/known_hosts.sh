#!/bin/sh

## Automaticaly add or remove the vagrant machine fingerprint from the known_hosts

# $1 = remove/add host from known_hosts
# $2 = the ip address
# $3 = the port

if [[ $2 =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then           # Test the ip adress format
  if [ $1 = 'remove' ]; then
    sed -i '' "/:$3/d" ~/.ssh/known_hosts                                       # For mac and BSD
    # sed -i "/:$1/d" ~/.ssh/known_hosts                                        # For linux
  elif [ $1 = 'add' ]; then
    ssh-keyscan -p $3 $2 >> ~/.ssh/known_hosts
  else
    echo 'Not a valid action (remove/add)'
  fi
else
  echo "Not an Ip Adress"
fi
